package org.gcube.gcat.api;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCatConstants {
	
	public static final String SERVICE_CLASS = "org.gcube.data-catalogue";
	public static final String SERVICE_NAME = "gcat";

	public final static String CONFIGURATION_CATEGORY = GCatConstants.SERVICE_CLASS;
	public final static String CONFIGURATION_NAME = GCatConstants.SERVICE_NAME + "-configuration";
	
	public static final String SERVICE_ENTRY_NAME = "org.gcube.gcat.ResourceInitializer";
	public static final String PURGE_QUERY_PARAMETER = "purge";
	public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";
	public static final String APPLICATION_JSON_API = "application/vnd.api+json";
	public static final String LIMIT_QUERY_PARAMETER = "limit";
	public static final String OFFSET_QUERY_PARAMETER = "offset";
	public static final String COUNT_QUERY_PARAMETER = "count";
	public static final String OWN_ONLY_QUERY_PARAMETER = "own_only";
	public static final String ALL_FIELDS_QUERY_PARAMETER = "all_fields";
	
	public static final String COUNT_KEY = "count";
	
	public static final String ORGANIZATION_PARAMETER = "organization";
	public static final String Q_KEY = "q";
	
	public static final String SOCIAL_POST_QUERY_PARAMETER = "social_post";
	public static final String SOCIAL_POST_NOTIFICATION_QUERY_PARAMETER = "social_post_notification";
	
	public static final String REFER_ONLY_RESOURCE_QUERY_PARAMETER = "refers_only";
	
	public static final String ORGANIZATION_FILTER_TEMPLATE = ORGANIZATION_PARAMETER + ":%s";
	
	public static final String JSON_API_DATA_FIELD_NAME = "data";
	
	
}
