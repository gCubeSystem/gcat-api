package org.gcube.gcat.api.moderation;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum CMItemVisibility {

	RESTRICTED("restricted"),
	PUBLIC("public");
	
	protected static final Map<String,CMItemVisibility> CM_ITEM_VISIBILITY_FROM_VALUE;
	
	static {
		CM_ITEM_VISIBILITY_FROM_VALUE = new HashMap<>();
		
		for(CMItemVisibility v : CMItemVisibility.values()) {
			CM_ITEM_VISIBILITY_FROM_VALUE.put(v.getValue(), v);
		}
	}
	
	protected final String value;
	
	private CMItemVisibility(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public static CMItemVisibility getCMItemStatusFromValue(String value) {
		return CM_ITEM_VISIBILITY_FROM_VALUE.get(value);
	}
}
