package org.gcube.gcat.api.moderation;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum CMItemStatus {

	PENDING("pending", true),
	APPROVED("approved", true),
	REJECTED("rejected", true),
	ANY("*", false);
	
	protected static final Map<String,CMItemStatus> CM_ITEM_STATUS_FROM_VALUE;
	
	static {
		CM_ITEM_STATUS_FROM_VALUE = new HashMap<>();
		
		for(CMItemStatus s : CMItemStatus.values()) {
			CM_ITEM_STATUS_FROM_VALUE.put(s.getValue(), s);
		}
	}
	
	protected final String value;
	protected final String fancyValue;
	protected final boolean validStatus;
	
	private CMItemStatus(String value, boolean validStatus) {
		this.value = value;
		if(this.value.length()>1) {
			this.fancyValue = Character.toUpperCase(value.charAt(0)) + value.substring(1);
		}else {
			this.fancyValue = this.value;
		}
		this.validStatus = validStatus;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getFancyValue() {
		return fancyValue;
	}
	
	public boolean isValidStatus() {
		return validStatus;
	}
	
	public static CMItemStatus getCMItemStatusFromValue(String value) {
		return CM_ITEM_STATUS_FROM_VALUE.get(value);
	}

}
