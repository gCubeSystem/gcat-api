package org.gcube.gcat.api.moderation;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public interface Moderated {
	
	public static final String CATALOGUE_MODERATOR = "Catalogue-Moderator";
	
	/* Content Moderation fields */
	public static final String CM_ITEM_STATUS_QUERY_PARAMETER = "status";
	public static final String SYSTEM_CM_PREFIX = "system:cm_";
	/**
	 * Used to keep tack of item status
	 */
	public static final String SYSTEM_CM_ITEM_STATUS = SYSTEM_CM_PREFIX + "item_status";
	/**
	 * Used to keep tack of item visibility
	 */
	public static final String SYSTEM_CM_ITEM_VISIBILITY = SYSTEM_CM_PREFIX + "item_visibility";
	/**
	 * Used to keep tack of item author
	 */
	public static final String SYSTEM_CM_ITEM_AUTHOR = SYSTEM_CM_PREFIX + "item_author";
	/**
	 * Used to keep tack of item author fullname
	 */
	public static final String SYSTEM_CM_ITEM_AUTHOR_FULLNAME = SYSTEM_CM_PREFIX + "item_author_fullname";

	
	
	public String approve(String moderatorMessage);
	
	public String reject(String moderatorMessage);
	
	public void message(String message);
	
}
