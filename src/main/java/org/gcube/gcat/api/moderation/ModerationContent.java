package org.gcube.gcat.api.moderation;

import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ModerationContent {

	@JsonProperty(value=Moderated.SYSTEM_CM_ITEM_STATUS,required=false)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	protected CMItemStatus cmItemStatus;
	
	@JsonProperty(required=false)
	protected String message;

	public CMItemStatus getCMItemStatus() {
		return cmItemStatus;
	}

	public void setCMItemStatus(CMItemStatus cmItemStatus) {
		this.cmItemStatus = cmItemStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
