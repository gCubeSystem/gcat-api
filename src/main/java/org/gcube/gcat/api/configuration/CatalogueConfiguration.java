package org.gcube.gcat.api.configuration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CatalogueConfiguration {

	public static final String ID_KEY = "id";
	public static final String CONTEXT_KEY = "context";
	public static final String DEFAULT_ORGANIZATION_KEY = "defaultOrganization";
	public static final String SUPPORTED_ORGANIZATIONS_KEY = "supportedOrganizations";
	
	public static final String SYS_ADMIN_TOKEN_KEY = "sysAdminToken";
	
	public static final String SOCIAL_POST_ENABLED_KEY = "socialPostEnabled";
	public static final String NOTIFICATION_TO_USER_ENABLED_KEY = "notificationToUsersEnabled";
	public static final String MODERATION_ENABLED_KEY = "moderationEnabled";
	
	public static final String CKAN_URL_KEY = "ckanURL";
	public static final String SOLR_URL_KEY = "solrURL";
	
	public static final String CKAN_DB_KEY = "ckanDB";
	
	public static final Set<String> KNOWN_PROPERTIES;
	
	static {
		KNOWN_PROPERTIES = new HashSet<>();
		KNOWN_PROPERTIES.add(ID_KEY);
		KNOWN_PROPERTIES.add(CONTEXT_KEY);
		KNOWN_PROPERTIES.add(DEFAULT_ORGANIZATION_KEY);
		KNOWN_PROPERTIES.add(SUPPORTED_ORGANIZATIONS_KEY);
		KNOWN_PROPERTIES.add(SYS_ADMIN_TOKEN_KEY);
		KNOWN_PROPERTIES.add(SOCIAL_POST_ENABLED_KEY);
		KNOWN_PROPERTIES.add(NOTIFICATION_TO_USER_ENABLED_KEY);
		KNOWN_PROPERTIES.add(MODERATION_ENABLED_KEY);
		KNOWN_PROPERTIES.add(CKAN_URL_KEY);
		KNOWN_PROPERTIES.add(SOLR_URL_KEY);
		KNOWN_PROPERTIES.add(CKAN_DB_KEY);
	}
	
	
	protected String id;
	
	protected String context;
	protected String defaultOrganization;
	protected Set<String> supportedOrganizations;
	
	protected String sysAdminToken;
	
	protected String ckanURL;
	protected String solrURL;
	
	protected boolean socialPostEnabled;
	protected boolean notificationToUsersEnabled;
	protected boolean moderationEnabled;
	
	protected Map<String,Object> additionalProperties;
	
	protected CKANDB ckanDB;
	
	/*
	 * Return the CKAN organization name using the current context name
	 */
	public static String getOrganizationName(String context) {
		String[] components = context.split("/");
		String name = components[components.length-1];
		return name.toLowerCase().replace(" ", "_");
	}

	protected CatalogueConfiguration() {
		this.supportedOrganizations = new HashSet<>();
		
		/* CKAN URL and sysAdminToken can only be retrieved from the IS */
		this.ckanURL = null;
		this.sysAdminToken = null;
		
		this.socialPostEnabled = true; // default is true
		this.notificationToUsersEnabled = false; // default is false
		
		this.moderationEnabled = false; // default is false
		
		additionalProperties = new HashMap<>();
		
	}
	
	public CatalogueConfiguration(String context) {
		this();
		setContext(context);
		this.defaultOrganization = CatalogueConfiguration.getOrganizationName(context);
		this.supportedOrganizations.add(defaultOrganization);
	}
	
	@JsonProperty(value = ID_KEY)
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	@JsonProperty(value = CONTEXT_KEY)
	public String getContext() {
		return context;
	}

	@JsonSetter
	private void setContext(String context) {
		this.context = context;
	}
	
	@JsonProperty(value = DEFAULT_ORGANIZATION_KEY)
	public String getDefaultOrganization() {
		if(defaultOrganization == null) {
			defaultOrganization = CatalogueConfiguration.getOrganizationName(context);
		}
		return defaultOrganization;
	}

	public void setDefaultOrganization(String defaultOrganization) {
		this.defaultOrganization = defaultOrganization;
	}

	@JsonProperty(value = SUPPORTED_ORGANIZATIONS_KEY)
	public Set<String> getSupportedOrganizations() {
		if(supportedOrganizations.isEmpty()) {
			supportedOrganizations.add(getDefaultOrganization());
		}
		return supportedOrganizations;
	}
	
	public void setSupportedOrganizations(Set<String> supportedOrganizations) {
		this.supportedOrganizations = supportedOrganizations;
	}
	
	@JsonProperty(value = SYS_ADMIN_TOKEN_KEY)
	public String getSysAdminToken() {
		return sysAdminToken;
	}

	public void setSysAdminToken(String sysAdminToken) {
		this.sysAdminToken = sysAdminToken;
	}
	
	@JsonProperty(value = CKAN_URL_KEY)
	public String getCkanURL() {
		return ckanURL;
	}

	public void setCkanURL(String ckanURL) {
		this.ckanURL = ckanURL;
	}
	
	@JsonProperty(value = SOLR_URL_KEY)
	public String getSolrURL() {
		return solrURL;
	}

	public void setSolrURL(String solrURL) {
		this.solrURL = solrURL;
	}

	@JsonProperty(value = SOCIAL_POST_ENABLED_KEY)
	public boolean isSocialPostEnabled() {
		return socialPostEnabled;
	}

	public void setSocialPostEnabled(boolean socialPostEnabled) {
		this.socialPostEnabled = socialPostEnabled;
	}
	
	@JsonProperty(value = NOTIFICATION_TO_USER_ENABLED_KEY)
	public boolean isNotificationToUsersEnabled() {
		return notificationToUsersEnabled;
	}

	public void setNotificationToUsersEnabled(boolean notificationToUsersEnabled) {
		this.notificationToUsersEnabled = notificationToUsersEnabled;
	}

	@JsonProperty(value = MODERATION_ENABLED_KEY)
	public boolean isModerationEnabled() {
		return moderationEnabled;
	}

	public void setModerationEnabled(boolean moderationEnabled) {
		this.moderationEnabled = moderationEnabled;
	}

	@JsonProperty(value = CKAN_DB_KEY)
	public CKANDB getCkanDB() {
		return ckanDB;
	}
	
	public void setCkanDB(CKANDB ckanDB) {
		this.ckanDB = ckanDB;
	}

	@JsonAnyGetter
	public Map<String,Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String,Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	
	public Object getAdditionalProperty(String key) {
		return this.additionalProperties.get(key);
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String key, Object value) {
		this.additionalProperties.put(key, value);
	}
	
}
