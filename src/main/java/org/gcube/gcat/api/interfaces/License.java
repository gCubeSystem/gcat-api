package org.gcube.gcat.api.interfaces;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface License {
	
	public static final String LICENSES = "licenses";

	public String list();
	
}
