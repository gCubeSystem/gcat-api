package org.gcube.gcat.api.interfaces;

import javax.xml.ws.WebServiceException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Organization<C,D> extends CRUD<C,D> {
	
	public static final String ORGANIZATIONS = "organizations";
	
	public int count() throws WebServiceException;
	
	public String list(int limit, int offset);
	
	public String patch(String name, String json);
	
}
