package org.gcube.gcat.api.interfaces;

import javax.xml.ws.WebServiceException;

public interface Trash<R> {

	public static final String TRASH = "trash";
	
	public String list(Boolean ownOnly) throws WebServiceException;
	
	public R empty(Boolean ownOnly) throws WebServiceException;
	
}
