package org.gcube.gcat.api.interfaces;

import javax.xml.ws.WebServiceException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Configuration<C,D> {
	
	public static final String CONFIGURATIONS = "configurations";
	
	public static final String CURRENT_CONTEXT_PATH_PARAMETER = "CURRENT_CONTEXT";
	
	public C create(String json) throws WebServiceException;
	
	public C read() throws WebServiceException ;

	public C update(String json) throws WebServiceException;
	
	public C patch(String json) throws WebServiceException;
	
	public D delete() throws WebServiceException;
	
	public D purge() throws WebServiceException;
	
}
