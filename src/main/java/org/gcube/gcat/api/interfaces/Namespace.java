package org.gcube.gcat.api.interfaces;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Namespace {
	
	public static final String NAMESPACES = "namespaces";

	public String list();
	
}
