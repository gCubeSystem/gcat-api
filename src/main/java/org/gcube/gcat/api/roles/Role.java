package org.gcube.gcat.api.roles;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum Role {
	
	MEMBER(Role.CATALOGUE_MEMBER, "member"), 
	EDITOR(Role.CATALOGUE_EDITOR, "editor"),
	ADMIN(Role.CATALOGUE_ADMIN, "admin"),
	MANAGER(Role.CATALOGUE_MANAGER, "admin");
	
	public static final String CATALOGUE_EDITOR = "Catalogue-Editor";
	public static final String CATALOGUE_MEMBER = "Catalogue-Member";
	public static final String CATALOGUE_ADMIN = "Catalogue-Admin";
	public static final String CATALOGUE_MANAGER = "Catalogue-Manager";
	
	private final String portalRole;
	private final String ckanRole;
	
	Role(String portalRole, String ckanRole) {
		this.portalRole = portalRole;
		this.ckanRole = ckanRole;
	}
	
	public String getPortalRole() {
		return portalRole;
	}
	
	public String getCkanRole() {
		return ckanRole;
	}
	
	protected static final Map<String,Role> ROLE_BY_PORTAL_ROLE;
	protected static final Map<String,Role> ROLE_BY_CKAN_ROLE;
	
	static {
		ROLE_BY_PORTAL_ROLE = new HashMap<String,Role>();
		
		// null or empty string identify a member
		ROLE_BY_PORTAL_ROLE.put(null, MEMBER);
		ROLE_BY_PORTAL_ROLE.put("", MEMBER);
		
		ROLE_BY_CKAN_ROLE = new HashMap<String,Role>();
		
		for(Role role : Role.values()) {
			ROLE_BY_PORTAL_ROLE.put(role.getPortalRole(), role);
			ROLE_BY_CKAN_ROLE.put(role.getCkanRole(), role);
		}
	}
	
	public static Role getRoleFromPortalRole(String portalRole) {
		return ROLE_BY_PORTAL_ROLE.get(portalRole);
	}
	
	public static String getCkanRoleFromPortalRole(String portalRole) {
		return getRoleFromPortalRole(portalRole).getCkanRole();
	}
	
	public static Role getRoleFromCkanRole(String ckanRole) {
		return ROLE_BY_CKAN_ROLE.get(ckanRole);
	}
	
	public static String getPortalRoleFromCkanRole(String ckanRole) {
		return getRoleFromCkanRole(ckanRole).getPortalRole();
	}
}