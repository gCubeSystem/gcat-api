# gCube Catalogue (gCat) API

gCube Catalogue (gCat) API is a library containing classes shared across gcat* components

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[gCube Catalogue (gCat) API](https://wiki.gcube-system.org/gcube/GCat_Service)

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/gcat-api/releases).

## Authors

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?

    @software{gcat-api,
		author		= {{Luca Frosini}},
		title		= {gCube Catalogue (gCat) API},
		abstract	= {gCube Catalogue (gCat) API is a library containing classes shared across gcat* components.},
		url			= {https://doi.org/10.5281/zenodo.7446431},
		keywords	= {Catalogue, D4Science, gCube}
	}
	
## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)


