This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Catalogue (gCat) API

## [v2.3.3-SNAPSHOT]

- Reordered constants to improve readability



## [v2.3.2]

- Library modified to be compliant with both Smartgears 3 and 4
- Added support for JSON:API on listing method of 'items' collection [#24601]
- Added support for JSON:API on listing method of 'licenses' collection [#24601]


## [v2.3.1] [r5.14.0] - 2022-12-07

- Added Item Author as system:cm_ metadata to keep track of item author


## [v2.3.0] [r5.13.1] - 2022-09-19

- Aligned constants name used as query parameters
- Added query parameter constant used in item listing to get the whole item instead of just the name [#23691]


## [v2.2.0] [r5.11.0] - 2022-05-12

- Added support to manage configurations [#22658]
- Migrated to ServiceClass corresponding to Maven groupId


## [v2.1.0] [r5.7.0] - 2022-01-27

- Added query parameter social_post_notification to override default VRE behaviour [#21345]
- Added support for moderation [#21342]
- Added items bulk delete/purge [#21685]
- Added empty trash API [#13322]


## [v2.0.0] [r5.2.0] - 2021-05-04

- Changed service class 

## [v1.2.2] [r5.0.0] - 2021-02-24

- Added count method for Item collection [#20627]
- Added count method for Organization, Group and Profile collection [#20629]
- Switched JSON management to gcube-jackson [#19735]


## [v1.2.1] [r4.18.0] - 2019-12-20

- Fixed distro files and pom according to new release procedure


## [v1.2.0] [r4.15.0] - 2019-11-19

- Added org parameter to support to list item at VO level in gCat Service [#17635]


## [v1.1.0] [r4.14.0] - 2019-05-16

- Added Social Post parameter [#13335]


## [v1.0.0] [r4.13.1] - 2019-02-26

- First Release

